package view;

import java.util.Scanner;

import model.data_structures.Lista;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import controller.Controller;

public class VistaManejadorPeliculas {
	
	public static void main(String[] args) {
		
		
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					Controller.cargarPeliculas();
					break;
				case 2:
					System.out.println("Ingrese subcadena de b�squeda:");
					String busqueda=sc.next();
					Lista<VOPelicula> lista=Controller.darListaPeliculas(busqueda);
					System.out.println("Se encontraron "+lista.darNumeroElementos()+" elementos");
					for (VOPelicula voPelicula : lista) {
						System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
					}
					break;
				case 3:
					System.out.println("Ingrese el a�o de b�squeda");
					int agno=sc.nextInt();
					Lista<VOPelicula> listaPeliculasAgno=Controller.darPeliculasPorAgno(agno);
					System.out.println("Se encontraron "+listaPeliculasAgno.darNumeroElementos()+" elementos");
					for (VOPelicula voPelicula : listaPeliculasAgno) {
						System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
					}
					break;
				case 4:
					VOAgnoPelicula agnoSiguiente=Controller.darPeliculasAgnoSiguiente();
					System.out.println("Las pel�culas del a�o "+agnoSiguiente.getAgno()+"son ");
					Lista<VOPelicula> listaPeliculasAgnoSiguiente=agnoSiguiente.getPeliculas();
					for (VOPelicula voPelicula : listaPeliculasAgnoSiguiente) {
						System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
					}
					break;
				case 5:
					VOAgnoPelicula agnoAnterior=Controller.darPeliculasAgnoAnterior();
					System.out.println("Las pel�culas del a�o "+agnoAnterior.getAgno()+"son ");
					Lista<VOPelicula> listaPeliculasAgnoAnterior=agnoAnterior.getPeliculas();
					for (VOPelicula voPelicula : listaPeliculasAgnoAnterior) {
						System.out.println(voPelicula.getTitulo()+" "+voPelicula.getAgnoPublicacion());
					}
					break;
				case 6:	
					fin=true;
					break;
			}
			
			
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("1. Cree una nueva colecci�n de pel�culas (data/movies.csv)");
		System.out.println("2. Buscar pel�culas por subcadena");
		System.out.println("3. Buscar pel�culas por a�o");
		System.out.println("4. Dar pel�culas a�o siguiente");
		System.out.println("5. Dar pel�culas a�o anterior");
		System.out.println("6. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
		
	}

}
