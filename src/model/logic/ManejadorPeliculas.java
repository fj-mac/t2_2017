package model.logic;

import model.data_structures.Lista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.awt.List;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private Lista<VOPelicula> misPeliculas;

	private Lista<VOAgnoPelicula> peliculasAgno;

	public final static String SEPARADOR = ",";
	
	int a�o;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub
		try
		{
			misPeliculas = new ListaEncadenada<VOPelicula>();
			peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
			String temp[] = new String[3];
			int agno;
			Lista<String> generos = new ListaEncadenada<>();

			File archivo = new File(archivoPeliculas);
			BufferedReader in = new BufferedReader(new FileReader(archivo));
			String linea = in.readLine();
			int i = 0;
			while((linea = in.readLine()) != null)
			{
				i++;
				if (linea.contains("\"")) 
				{
					temp[1] = linea.substring(linea.indexOf(',') + 2, linea.lastIndexOf(',') - 1);
					temp[2] = linea.substring(linea.lastIndexOf(',') + 1);
				} 
				else 
				{
					temp = linea.split(",");
				}
				try 
				{
					agno = Integer.parseInt(temp[1].substring(temp[1].lastIndexOf('(')+1, temp[1].lastIndexOf(')')));
				} 
				catch (NumberFormatException | StringIndexOutOfBoundsException e) 
				{
					if (temp[1].endsWith(")")) 
					{
						agno = Integer.valueOf(temp[1].substring(temp[1].lastIndexOf('(') + 1, temp[1].lastIndexOf('-')));
					} 
					else 
						agno = 0;
				}
				String[] partesGeneros = temp[2].split("\\|");
				generos = new ListaEncadenada<>();
				for( String p: partesGeneros)
				{
					generos.anexarElementoFinal(p);
				}
				
				if( temp[1].endsWith(")"))
				{
					temp[1] = temp[1].substring(0, temp[1].lastIndexOf('(') - 1);
				}
				
				System.out.println(i + ": " + temp[1] + "(" + agno + ")" + " - " + generos.darElemento(0)  );

				VOPelicula pelicula = new VOPelicula();
				pelicula.setTitulo(temp[1]);
				pelicula.setAgnoPublicacion(agno);
				pelicula.setGenerosAsociados(generos);
				misPeliculas.anexarElementoFinal(pelicula);

			}

			VOAgnoPelicula anio;
			for( int j = 1950; j <= 2016; j++ )
			{
				anio = new VOAgnoPelicula();
				anio.setAgno(j);
				ListaEncadenada<VOPelicula> lista = new ListaEncadenada<>();
				for( int k = 0; k < misPeliculas.darNumeroElementos(); k++ )
				{

					if( misPeliculas.darElemento(k).getAgnoPublicacion() == j )
					{
						lista.anexarElementoFinal(misPeliculas.darElemento(k));
					}
				}
				anio.setPeliculas(lista);
				peliculasAgno.anexarElementoFinal(anio);
			}
		}
		catch( IOException e )
		{

		}

	}

	@Override
	public Lista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		Lista<VOPelicula> ordenada = new ListaEncadenada<>();
		int cont=0;
		VOPelicula actual= misPeliculas.darElemento(0);

		for (int i=0;i<misPeliculas.darNumeroElementos();i++)
		{
			if (actual.getTitulo().contains(busqueda))
			{
				ordenada.anexarElementoFinal(actual);
			}
			cont=cont+1;
			actual=misPeliculas.darElemento(cont);
		}
		return ordenada;
	}

	@Override
	public Lista<VOPelicula> darPeliculasAgno(int agno) {
		// TODO Auto-generated method stub
		Lista<VOPelicula> lista = new ListaDobleEncadenada<>();
		VOAgnoPelicula actual = peliculasAgno.darElemento(0);
		int contador = 0;
		a�o=agno;

		for( int i = 0; i < peliculasAgno.darNumeroElementos(); i++)
		{
			if( actual.getAgno() == agno )
			{
				lista = actual.getPeliculas();
			}
			contador++;
			actual = peliculasAgno.darElemento(contador);
		}
		return lista;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		try
		{
			return (VOAgnoPelicula) darPeliculasAgno(a�o+1);
		}
		catch (Exception e)
		{
			return null;
		}
		
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		try
		{
			return (VOAgnoPelicula) darPeliculasAgno(a�o-1);
		}
		catch (Exception e)
		{
			return null;
		}
	}

}
