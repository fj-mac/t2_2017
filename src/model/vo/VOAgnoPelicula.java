package model.vo;

import model.data_structures.Lista;

public class VOAgnoPelicula {

	private int agno;
	private Lista<VOPelicula> peliculas;
	public int getAgno() {
		return agno;
	}
	public void setAgno(int agno) {
		this.agno = agno;
	}
	public Lista<VOPelicula> getPeliculas() {
		return peliculas;
	}
	public void setPeliculas(Lista<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	}
	
	
}
