package model.vo;

import model.data_structures.Lista;

public class VOPelicula {
	
	private String titulo;
	private int agnoPublicacion;
	private Lista<String> generosAsociados;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	
	public Lista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(Lista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	

}
