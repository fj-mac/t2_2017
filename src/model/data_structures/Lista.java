package model.data_structures;

public interface Lista<T> extends Iterable<T>{
	

	public void anexarElementoFinal(T elem);
	
	public T darElemento(int pos);
	
	public int darNumeroElementos();
	
	public T darElementoPosicionActual();
	
	public void avanzarSiguientePosicion();
	
	public void retrocederPosicionAnterior();
	
	
	
	
	

}
