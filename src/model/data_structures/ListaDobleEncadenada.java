package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements Lista<T> {
	NodoDoble<T> actual;
	NodoDoble<T> primero;
	NodoDoble<T> anterior;
	int num = 0;
	@Override
	public Iterator<T> iterator() {

		class iterador<T> implements Iterator{
			NodoDoble<T> a;


			iterador(NodoDoble<T> p)
			{
				a =p;
			}

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				if(a==null)
					return false;
				else
					return true;
			}

			@Override
			public Object next() {
				// TODO Auto-generated method stub
				if (this.hasNext()==true)
				{
					NodoDoble<T> temporal;
					temporal=a;
					a=a.darSiguiente();
					return temporal;
				}
				return null;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub


			}
		}
		return null;
	}

	@Override
	public void anexarElementoFinal(T elem) {
		NodoDoble<T> nuevo = new NodoDoble<T>(elem);
		if( primero == null )
		{
			primero = nuevo;
		}
		else
		{
			NodoDoble<T> nod = primero;
			while(nod.darSiguiente()!=null){
				nod = nod.darSiguiente();
			}

			nuevo.agregarAnterior(nod);
			nod.agregarSiguiente(nuevo);
		}
		num++;
	}

	@Override
	public T darElemento(int pos) {
		int cont=0;
		NodoDoble<T> nod = primero;

			if(pos==0)
			{
				return nod.darElemento();
			}
			else
			{


				while(nod.darSiguiente()!=null){
					nod = nod.darSiguiente();
					cont=cont+1;
					if (cont == pos)
					{
						return nod.darElemento();

					}
				}
			}



			return null;

	}


	@Override
	public int darNumeroElementos() {
	return num;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return (T) actual;
	}

	@Override
	public void avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		actual=actual.darSiguiente();

	}

	@Override
	public void retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		actual=actual.darAnterior();

	}

}
