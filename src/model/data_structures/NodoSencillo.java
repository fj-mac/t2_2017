package model.data_structures;

public class NodoSencillo <T>{


	private NodoSencillo<T> siguiente;

	private T objeto;

	public NodoSencillo ( T pObjeto)
	{
		siguiente=null;

		objeto=pObjeto;
	}



	public void agregarSiguiente(NodoSencillo<T> pSiguiente)
	{
		siguiente=pSiguiente;
	}
	public NodoSencillo darSiguiente()
	{
		return siguiente;
	}
	
	public T darElemento()
	{
		return objeto;
	}



}
